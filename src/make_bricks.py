"""Make brik"""


def make_bricks(small, big, goal):
    big_count = goal // 5
    if big <= big_count:
        big_count = big
    goal -= big_count * 5
    if goal < small:
        return True
    return False

