"""
Choose student
"""
import csv
import datetime

def get_present_students(date=datetime.datetime.now()):
    """
    Get students present at date passed
    :return:
    """

    # open file
    with open('../data/students.csv') as file:
        csv_reader = csv.reader(file, delimiter=',')
        for row in csv_reader:
            print(row)


get_present_students()