"""
Tests
"""
import unittest

from src.is_leap_year import is_leap


class LeapTestCase(unittest.TestCase):

    def test_leap(self):
        """
        Test is_leap function
        :return:
        """
        self.assertTrue(is_leap(2000))
        self.assertFalse(is_leap(2001))
        self.assertFalse(is_leap(1800))


